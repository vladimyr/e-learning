<?php
require_once __DIR__ . '/vendor/autoload.php';

$klein = new \Klein\Klein();

$klein->onError(function ($klein, $msg, $type, $err) {
  if ($err instanceof \Klein\Exceptions\ValidationException) {
    $klein->response()->code(400)->json([
      "success" => False,
      "error" => $msg
    ]);
    return;
  }
});


$klein->respond('GET', '/hello-world', function () {
  return 'Hello World!';
});

$klein->with('/api', 'routes.php');


$klein->dispatch();
