<?php

$klein = $this;

$klein->post('/login', function ($request, $response, $service) {
  $service->validateParam('username', 'Username is required!')->notNull();
  $service->validateParam('password', 'Password is required!')->notNull();

  $username = $request->username;
  $password = $request->password;

  $service->startSession();
  return $response->json([
    "username" => $username,
    "password" => $password
  ]);
});

$klein->get('/info', function ($request) {
  $session = $request->session();
});

$klein->post('/logout', function () {
  return 'logut';
});

// echo phpinfo();
